#!/bin/sh

make fanout || exit 1

epoch=0
pkgname=`awk '/^Package:/ {print $2}' debian2/control`
version=`awk '/^Version:/ {print $2}' debian2/control`
pkgdir=tmp/stage/$pkgname/$epoch/$version

bindir=$pkgdir/data/usr/local/sbin
mkdir -p $pkgdir/control

cp debian2/control $pkgdir/control
echo "2.0" >$pkgdir/debian-binary

mkdir -p $bindir
cp fanout $bindir/

echo mkdeb.pl --gz $pkgdir $*
mkdeb.pl --gz $pkgdir $*

